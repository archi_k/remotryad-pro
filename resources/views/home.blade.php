@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    You are logged in!
                </div>
                {!! Form::open(['method' => 'POST', 'route' => 'logout', 'class' => 'form-horizontal']) !!}
                    <div class="btn-group pull-right">
                        {!! Form::submit("Logout", ['class' => 'btn btn-success']) !!}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
