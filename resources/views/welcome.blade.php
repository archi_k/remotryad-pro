@extends('layouts.app')
@section('content')
  <div id="hero" class="carousel slide" data-ride="carousel" data-interval="10000">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#hero" data-slide-to="0" class="active"></li>
        <li data-target="#hero" data-slide-to="1"></li>
        <li data-target="#hero" data-slide-to="2"></li>
      </ol>

      <!-- Wrapper for slides -->
      <div class="carousel-inner" role="listbox">
        <div id="slide-1" class="item active">
          <div class="container vertical-align">
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-5">
                <div id="s1-b1">
                  <div style="width: 100%; height: auto; padding: 20px; background-color: rgba(246,206,24,1); font-family: HelveticaNeue-Light; color: #010103;" class="text-center">
                    <h1 class="font-Roboto">Закажите <span style="color: white">ремонт квартиры</span> без навязанных затрат</h1>
                    <h2>от 2 295 р./кв.м <span style="color: red;text-decoration: line-through;">3 200</span> р./кв.м</h2>
                    <p>Ремонт квартир "под ключ" в Фрязино, Щелково, Литвиново</p>
                  </div>
                </div>
              </div>
              <div class="col-md-7"></div>
            </div>
          </div>
        </div>
        <div id="slide-2" class="item">
            <div class="container vertical-align">
              <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                  <discount-request keep-alive></discount-request>
                </div>
                <div class="col-md-3"></div>
              </div>
            </div>
        </div>
        <div id="slide-3" class="item">
            <div class="container vertical-align">
              <div class="row">
                  <div class="col-md-2"></div>
                  <div class="col-xs-12 col-sm-12 col-md-8">
                      <price-calculation keep-alive></price-calculation>
                  </div>
                  <div class="col-md-2"></div>
              </div>
            </div>
        </div>
      </div>

      <!-- Controls -->
      <a class="left carousel-control" href="#hero" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#hero" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
  </div>
  <advantages></advantages>
  <tariffs></tariffs>
  <calculator></calculator>
  <portfolio></portfolio>
  <request></request>
@endsection
