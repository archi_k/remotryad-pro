<!DOCTYPE html>
<html lang="{{config('app.locale')}}">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{config('app.name')}}</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <!--[if lt IE 9]>
      <script src="https://cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div id="app">
      <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{URL::route('welcome')}}">{{config('app.name')}}</a>
            <p class="navbar-text">+7 (903) 222-07-05</p>
          </div>
          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="navbar">
            <button class="btn btn-success navbar-btn navbar-right" data-toggle="modal" data-target="#master-request-form">Вызвать мастера</button>
            <ul class="nav navbar-nav navbar-right">
              <li><a href="#hero"><span class="glyphicon glyphicon-home small"></span></a></li>
              <li><a href="#advantages">Преимущества</a></li>
              <li><a href="#tariffs">Тарифы</a></li>
              <li><a href="#calculator">Калькулятор</a></li>
              <li><a href="#portfolio">Примеры и Отзывы</a></li>
            </ul>
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>
      @yield('content')
    </div>
    <div class="container-fluid" id="footer-block">
      <div class="container">
        <div class="row">
          <div class="col-md-3">
            <h4 class="font-Roboto">О нас</h4>
            <p class="small text-muted">«Ремотряд» выполняет простой и комплексный ремонт квартир под ключ в городах подмосковья Щелково, Фрязино, Литвиново</p>
            <p class="small text-muted">Все материалы сайта принадлежат их авторам.<br>Указание ссылок на материалы сайта обязательно.</p>
            <p class="small text-muted">© 2007 - {{ Carbon\Carbon::now()->format('Y') }} &laquo;Ремотряд&raquo;</p>
          </div>
          <div class="col-md-3">
            <h4 class="font-Roboto">Последние проекты</h4>
          </div>
          <div class="col-md-3">
            <h4 class="font-Roboto">Информация</h4>
            <ul class="small text-muted">
              <li><a href="#advantages">Преимущества</a></li>
              <li><a href="#tariffs">Тарифы</a></li>
              <li><a href="#calculator">Калькулятор</a></li>
              <li><a href="#portfolio">Примеры и Отзывы</a></li>
            </ul>
          </div>
          <div class="col-md-3 pull-right">
            <h4 class="font-Roboto">Подпишитесь и будьте в курсе</h4>
            <a class="btn btn-social-icon btn-vk">
              <span class="fa fa-vk"></span>
            </a>
            <a class="btn btn-social-icon btn-odnoklassniki">
              <span class="fa fa-odnoklassniki"></span>
            </a>
            <a class="btn btn-social-icon btn-twitter">
              <span class="fa fa-twitter"></span>
            </a>
            <a class="btn btn-social-icon btn-facebook">
              <span class="fa fa-facebook"></span>
            </a>
            <a class="btn btn-social-icon btn-instagram">
              <span class="fa fa-instagram"></span>
            </a>
          </div>
        </div>
      </div>
    </div>
    <script src="{{asset('js/app.js')}}"></script>
  </body>
</html>
