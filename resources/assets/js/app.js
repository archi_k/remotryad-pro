/*jshint esversion: 6 */
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('jquery-ui-bundle');
require('./bootstrap');
require('bootstrap-formhelpers/js/bootstrap-formhelpers-phone');
require('bootstrap-formhelpers/js/bootstrap-formhelpers-countries');
require('bootstrap-formhelpers/js/bootstrap-formhelpers-number');
require('vue-resource');
require('vue-cookie');
require('axios');

window.Vue = require('vue');
var VueCookie = require('vue-cookie');
Vue.use(VueCookie);
Vue.use(require('vue-moment'));

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('passport-clients', require('./components/passport/Clients.vue'));
Vue.component('passport-authorized-clients',require('./components/passport/AuthorizedClients.vue'));
Vue.component('passport-personal-access-tokens',require('./components/passport/PersonalAccessTokens.vue'));

Vue.component('discount-request', require('./components/discount-request.vue'));
Vue.component('price-calculation', require('./components/price-calculation.vue'));
Vue.component('advantages', require('./components/advantages.vue'));
Vue.component('tariffs', require('./components/tariffs.vue'));
Vue.component('calculator', require('./components/calculator.vue'));
Vue.component('portfolio', require('./components/portfolio.vue'));
Vue.component('request', require('./components/request.vue'));

const app = new Vue({el: '#app'});

$(document).ready(function() {

  // Menu scroll buttons
  function scrollNav() {
    $('.nav a').click(function() {
      $('html, body').stop().animate({
        scrollTop: $($(this).attr('href')).offset().top - 50
      }, 400);
      return false;
    });
    $('.scrollTop a').scrollTop();
  }

  function scrollNavButton() {
    $('a.btn.btn-success.navbar-btn.navbar-right').click(function() {
      //Animate
      $('html, body').stop().animate({
        scrollTop: $($(this).attr('href')).offset().top - 60 // padding-top
      }, 400);
      return false;
    });
    $('.scrollTop a').scrollTop();
  }

  scrollNav();
  scrollNavButton();

  // initialize Carousel
  $('.carousel').carousel();

  setInterval(function() {
    $('#phone-number').effect("bounce", {
      times: 3
    }, 500);
  }, 10000);

  // Add shadow to navbar on scroll
  var scroll_start = 0;
  var startchange = $('#hero');
  var offset = startchange.offset();
  if (startchange.length) {
   $(document).scroll(function() {
      scroll_start = $(this).scrollTop();
      if(scroll_start > offset.top) {
        $(".navbar").css('box-shadow', '1px 1px 15px #888888');
       } else {
        $(".navbar").css('box-shadow', '0px 0px 0px #888888');
       }
   });
  }

});
