<?php

namespace App\Http\Controllers;

use App\DiscountRequest;
use Illuminate\Http\Request;

class DiscountRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if ($request->has('phone_number')) {

        $data = [];
        $message = '';

        // Check if phone number already been sent and existed in database

        $phone_in_db = DiscountRequest::whereIn('phone_number',$request->all())->first();

        // If number already exists return message 'Already requested'
        if (null !== $phone_in_db) {
            $message = "Ваша заявка принята";
        } else {
            // If numer is new save it to database
            $discount_request = new DiscountRequest($request->all());
            $discount_request->save();
            $message = "Ваша завка принята";
        }

        $data['message'] = $message;

      } else {

        $error = 'Упс! что-то пошло не так. Мы работаем над устранением проблемы.';

        $data['error'] = $error;
      }

      return $data;

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DiscountRequest  $discountRequest
     * @return \Illuminate\Http\Response
     */
    public function show(DiscountRequest $discountRequest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DiscountRequest  $discountRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(DiscountRequest $discountRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DiscountRequest  $discountRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DiscountRequest $discountRequest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DiscountRequest  $discountRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(DiscountRequest $discountRequest)
    {
        //
    }
}
