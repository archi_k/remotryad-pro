<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiscountRequest extends Model
{
     protected $fillable = ['phone_number'];
}
