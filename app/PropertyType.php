<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Projects;


class PropertyType extends Model
{
  public function project()
  {
    return $this->belongsToMany(Project::class, 'projects');
  }
}
