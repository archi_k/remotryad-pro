<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Tariff extends Model
{
  public function project()
  {
    return $this->belongsToMany(Project::class, 'projects');
  }
}
