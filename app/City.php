<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Project;


class City extends Model
{
  public function project()
  {
    return $this->belongsToMany(Project::class, 'projects');
  }
}
