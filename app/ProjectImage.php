<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \App\Project;

class ProjectImage extends Model
{
  public function projectid()
  {
    return $this->belongsTo(Project::class, 'projects');
  }
}
