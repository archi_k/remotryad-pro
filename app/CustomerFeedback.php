<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class CustomerFeedback extends Model
{
  public function projectid()
  {
    return $this->belongsTo(Project::class,'projects');
  }
}
