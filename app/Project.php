<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \App\ProjectImage;
use \App\CustomerFeedback;


class Project extends Model
{
  public function projectimage()
  {
    return $this->hasMany(ProjectImage::class);
  }

  public function city()
  {
    return $this->hasOne(city::class,'id');
  }

  public function tariff()
  {
    return $this->hasOne(Tariff::class, 'id');
  }

  public function propertytype()
  {
    return $this->hasOne(PropertyType::class,'id');
  }

  public function customerfeedback()
  {
    return $this->hasOne(CustomerFeedback::class,'project_id');
  }

}
